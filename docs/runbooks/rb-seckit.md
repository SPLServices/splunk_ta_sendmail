# Using OOB Configuration

## Requirements

* Supported Operating System
* Splunk Enterprise or Splunk Cloud
* Complete deployment of Splunk_TA_nix

## Splunk Cloud

1. Open a TA deployment request for `Splunk_TA_Sendmail`

## Splunk Enterprise Cluster Master

1. Download latest `Splunk_TA_Sendmail-*_indexers.tar.gz`
2. Splunk Cluster Master unzip to master-apps
3. Deploy one of create an index "mta" or a index named per local standards
	4. `SecKit_splunk_index_2_Sendmail_home-<latest>.tar.gz`
	5. `SecKit_splunk_index_2_Sendmail_vol-<latest>.tar.gz`

## Splunk Enterprise Indexers and Intemediate Forwarders

1. Download latest `Splunk_TA_Sendmail-*_indexers.tar.gz`
2. Login (sudo) to the splunk user `sudo su - splunk`
3. Install the app `splunk app install <filename>`
4. Define custom indexes as neded
4. Restart splunk `systemctl restart splunk`
5. Deploy one of
	4. `Splunk_TA_Sendmail-<latest>.tar.gz`
	5. `Splunk_TA_Sendmail<latest>-<latest>.tar.gz`

## Splunk Enterprise Search Head

1. Download latest `Splunk_TA_Sendmail-<latest>-_search_heads.tar.gz`
2. Install the app `splunk app install <filename>`

## Source Operating System Configuration

### General

Pre-implementation Procedure

1. Verify the sendmail instance utilize the default log location `/var/log/sendmail/sendmail*.log*`

## Splunk Deployment Server

none

## Post Install Validation

1. Run the following search or derivative based on deployment scope

```| tstats count where index=mta by host```
